package com.lcs.lottery.utils;

/**
 * Created by Anton Shvechikov on 10.04.16.
 */
public class StringUtils {

    public static final String COMMA_DELIMITER = ",";

    /**
     * Returns string without empty spaces
      * @param str - specified string
     * @return String without empty spaces
     */
    public static String getStringWithoutEmptySpaces(String str) {
        return str != null ? str.replace(" ", "") : null;
    }
}
