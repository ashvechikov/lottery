package com.lcs.lottery.utils;

import static java.lang.Math.max;

/**
 * Created by Anton Shvechikov on 10.04.16.
 */
public class LCS {

    /**
     * Compute Longest common subsequence dynamically
     * @param str - first string
     * @param comparing - string to compare
     * @return - size of LCS
     */
    public static int computeLCSSize(String str, String comparing) {
        if (str != null && comparing != null && !str.trim().isEmpty() && !comparing.trim().isEmpty()) {
            int[][] lengths = computeLCSTable(str, comparing);
            return countLCSFromTable(str, comparing, lengths);
        }
        return 0;
    }

    // read the substring out from the matrix
    private static int countLCSFromTable(String str, String comparing, int[][] lengths) {
        int size = 0;
        for (int x = str.length(), y = comparing.length(); x != 0 && y != 0; ) {
            if (lengths[x][y] == lengths[x - 1][y]) {
                x--;
            } else if (lengths[x][y] == lengths[x][y - 1]) {
                y--;
            } else {
                if (str.charAt(x - 1) == comparing.charAt(y - 1)) {
                    size++;
                }
                x--;
                y--;
            }
        }
        return size;
    }

    private static int[][] computeLCSTable(String str, String comparing) {
        int[][] lengths = new int[str.length()+1][comparing.length()+1];

        for (int i = 0; i < str.length(); i++) {
            for (int j = 0; j < comparing.length(); j++) {
                if (str.charAt(i) == comparing.charAt(j)) {
                    lengths[i + 1][j + 1] = lengths[i][j] + 1;
                } else {
                    lengths[i + 1][j + 1] = max(lengths[i + 1][j], lengths[i][j + 1]);
                }
            }
        }
        return lengths;
    }
}
