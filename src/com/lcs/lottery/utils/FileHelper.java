package com.lcs.lottery.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

/**
 * Created by Anton Shvechikov on 10.04.16.
 */
public class FileHelper {

    /**
     * Gets all lines from file. If file wan't  found - shows message and exits programm
     * @param filePath - specified file path
     * @return List of lines if file is found, otherwise empty list
     */
    public static List<String> readAll(String filePath) {
        if (filePath != null && !filePath.isEmpty()) {
            Path path = Paths.get(filePath);
            try {
                return Files.lines(path).collect(toList());
            } catch (IOException e) {
                System.out.println("File not found or corrupted! " + e.getMessage());
                System.exit(1);
            }
        }
        return emptyList();
    }
}
