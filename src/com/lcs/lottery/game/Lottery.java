package com.lcs.lottery.game;

import com.lcs.lottery.user.Participant;
import com.lcs.lottery.utils.FileHelper;
import com.lcs.lottery.utils.LCS;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import static com.lcs.lottery.utils.StringUtils.COMMA_DELIMITER;
import static com.lcs.lottery.utils.StringUtils.getStringWithoutEmptySpaces;

/**
 * Created by Anton Shvechikov on 10.04.16.
 */
public class Lottery {

    private static final String WINNING_NUMBER_STRING = "Winning number:";
    private static final int INFO_LINES_SIZE = 4;
    /**
     * Positions for each item inside string
     */
    private static final int LAST_NAME_POSITION = 0;
    private static final int NAME_POSITION = 1;
    private static final int COUNTRY_POSITION = 2;
    private static final int TICKETS_POSITION = 3;

    private Map<Participant, Integer> participantsGame;
    private String winningNumber;
    private String filePath;

    public Lottery(String filePath) {
        this.filePath = filePath;
        this.participantsGame = new TreeMap<>((o1, o2) -> {
            int compareLastName = o1.getLastName().compareTo(o2.getLastName());
            if (compareLastName != 0) {
                return compareLastName;
            } else {
                int compareName = o1.getName().compareTo(o2.getName());
                if (compareName != 0) {
                    return compareName;
                } else {
                    return o1.getCounty().compareTo(o2.getCounty());
                }
            }
        });
    }

    /**
     * Computes who won and how many tickets won
     */
    public void computeWinners() {
        if (filePath != null && !filePath.isEmpty()) {
            List<String> lines = FileHelper.readAll(filePath);
            if (setWinningNumber(lines)) {
                lines.stream().forEach(line -> {
                    String[] values = line.split(COMMA_DELIMITER);
                    if (values.length == INFO_LINES_SIZE) {
                        Participant participant = new Participant(values[LAST_NAME_POSITION], values[NAME_POSITION],
                                values[COUNTRY_POSITION]);
                        String tickets = values[TICKETS_POSITION];
                        updateWins(participant, tickets);
                    }
                });
                removeLoosers();
            } else {
                System.out.println("Couldn't find the winning number line!");
                System.exit(1);
            }
        }
    }

    /**
     * Print out all the winners by asc by LastName, Name, Country, Amount of lucky tickets
     */
    public void printWinners() {
        for (Map.Entry<Participant, Integer> entry : participantsGame.entrySet()) {
            System.out.println(entry.getKey() + COMMA_DELIMITER + entry.getValue());
        }
    }

    private void removeLoosers() {
        participantsGame.entrySet().removeIf(entry -> entry.getValue() == 0);
    }

    private boolean setWinningNumber(List<String> lines) {
        Optional<String> winning = lines.stream().filter(s -> s.contains(WINNING_NUMBER_STRING)).findFirst();
        if (winning.isPresent()) {
            String[] strings = winning.get().split(WINNING_NUMBER_STRING);
            if (strings.length == 2) {
                winningNumber = getStringWithoutEmptySpaces(strings[1].trim());
                return true;
            }
        }
        return false;
    }

    private void updateWins(Participant participant, String tickets) {
        if (participantsGame.containsKey(participant)) {
            Integer winsCount = participantsGame.get(participant);
            participantsGame.put(participant, LCS.computeLCSSize(tickets, winningNumber) + winsCount);
        } else {
            participantsGame.put(participant, LCS.computeLCSSize(tickets, winningNumber));
        }
    }
}
