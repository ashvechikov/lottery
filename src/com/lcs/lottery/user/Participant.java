package com.lcs.lottery.user;

import static com.lcs.lottery.utils.StringUtils.COMMA_DELIMITER;

/**
 * Created by Anton Shvechikov on 10.04.16.
 */
public class Participant {

    private String name;
    private String lastName;
    private String county;

    public Participant(String lastName, String name, String county) {
        this.lastName = lastName;
        this.name = name;
        this.county = county;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCounty() {
        return county;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Participant that = (Participant) o;

        if (getName() != null ? !getName().equals(that.getName()) : that.getName() != null) return false;
        if (getLastName() != null ? !getLastName().equals(that.getLastName()) : that.getLastName() != null)
            return false;
        return !(getCounty() != null ? !getCounty().equals(that.getCounty()) : that.getCounty() != null);

    }

    @Override
    public int hashCode() {
        int result = getName() != null ? getName().hashCode() : 0;
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getCounty() != null ? getCounty().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return lastName + COMMA_DELIMITER +  name + COMMA_DELIMITER + county;
    }
}
