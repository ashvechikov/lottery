package com.lcs.lottery;

import com.lcs.lottery.game.Lottery;

/**
 * Created by Anton Shvechikov on 10.04.16.
 */
public class Starter {

    public static void main(String[] args) {
        Lottery lottery = new Lottery("resources/input.txt");
        lottery.computeWinners();
        lottery.printWinners();
    }
}
